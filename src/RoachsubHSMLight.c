/*
 * File: TemplateSubHSM.c
 * Author: J. Edward Carryer
 * Modified: Gabriel H Elkaim
 *
 * Template file to set up a Heirarchical State Machine to work with the Events and
 * Services Framework (ES_Framework) on the Uno32 for the CMPE-118/L class. Note that
 * this file will need to be modified to fit your exact needs, and most of the names
 * will have to be changed to match your code.
 *
 * There is for a substate machine. Make sure it has a unique name
 *
 * This is provided as an example and a good place to start.
 *
 * History
 * When           Who     What/Why
 * -------------- ---     --------
 * 09/13/13 15:17 ghe      added tattletail functionality and recursive calls
 * 01/15/12 11:12 jec      revisions for Gen2 framework
 * 11/07/11 11:26 jec      made the queue static
 * 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 * 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 */


/*******************************************************************************
 * MODULE #INCLUDE                                                             *
 ******************************************************************************/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "BOARD.h"
#include "RoachHSM.h"
#include "RoachsubHSMLight.h"

/*******************************************************************************
 * MODULE #DEFINES                                                             *
 ******************************************************************************/
typedef enum {
    //Init,
    Run,
    Backup,
    Turn,
    Dance,
} LightSubHSMState_t;

static const char *StateNames[] = {
	"Run",
	"Backup",
	"Turn",
	"Dance",
};



/*******************************************************************************
 * PRIVATE FUNCTION PROTOTYPES                                                 *
 ******************************************************************************/
/* Prototypes for private functions for this machine. They should be functions
   relevant to the behavior of this state machine */
static void RunS(void){
    Roach_LeftMtrSpeed(LEFTSCALE * RUN);
    Roach_RightMtrSpeed(RIGHTSCALE * RUN);
}
static void WalkS(void){
    Roach_LeftMtrSpeed(LEFTSCALE * WALK);
    Roach_RightMtrSpeed(RIGHTSCALE * WALK);
}
static void BackUP(void){
    Roach_LeftMtrSpeed(LEFTSCALE * -WALK);
    Roach_RightMtrSpeed(RIGHTSCALE * -WALK);
}
static void TurnR(void){
    Roach_LeftMtrSpeed(LEFTSCALE * RUN);
    Roach_RightMtrSpeed(RIGHTSCALE * -RUN);
}
static void TurnL(void){
    Roach_LeftMtrSpeed(LEFTSCALE * -RUN);
    Roach_RightMtrSpeed(RIGHTSCALE * RUN);
}
/*******************************************************************************
 * PRIVATE MODULE VARIABLES                                                            *
 ******************************************************************************/
/* You will need MyPriority and the state variable; you may need others as well.
 * The type of state variable should match that of enum in header file. */

static LightSubHSMState_t CurrentState = Run; // <- change name to match ENUM
static uint8_t MyPriority;
static ES_Event BumperEvent;

/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/

/**
 * @Function InitTemplateSubHSM(uint8_t Priority)
 * @param Priority - internal variable to track which event queue to use
 * @return TRUE or FALSE
 * @brief This will get called by the framework at the beginning of the code
 *        execution. It will post an ES_INIT event to the appropriate event
 *        queue, which will be handled inside RunTemplateFSM function. Remember
 *        to rename this to something appropriate.
 *        Returns TRUE if successful, FALSE otherwise
 * @author J. Edward Carryer, 2011.10.23 19:25 */
uint8_t InitRoachSubLightHSM(void)
{
    ES_Event returnEvent;

    CurrentState = Run;
    returnEvent = RunRoachSubLightHSM(INIT_EVENT);
    if (returnEvent.EventType == ES_NO_EVENT) {
        return TRUE;
    }
    return FALSE;
}

/**
 * @Function RunTemplateSubHSM(ES_Event ThisEvent)
 * @param ThisEvent - the event (type and param) to be responded.
 * @return Event - return event (type and param), in general should be ES_NO_EVENT
 * @brief This function is where you implement the whole of the heirarchical state
 *        machine, as this is called any time a new event is passed to the event
 *        queue. This function will be called recursively to implement the correct
 *        order for a state transition to be: exit current state -> enter next state
 *        using the ES_EXIT and ES_ENTRY events.
 * @note Remember to rename to something appropriate.
 *       The lower level state machines are run first, to see if the event is dealt
 *       with there rather than at the current level. ES_EXIT and ES_ENTRY events are
 *       not consumed as these need to pass pack to the higher level state machine.
 * @author J. Edward Carryer, 2011.10.23 19:25
 * @author Gabriel H Elkaim, 2011.10.23 19:25 */
ES_Event RunRoachSubLightHSM(ES_Event ThisEvent)
{
    uint8_t makeTransition = FALSE; // use to flag transition
    LightSubHSMState_t nextState; // <- change type to correct enum

    ES_Tattle(); // trace call stack

    switch (CurrentState) {
        case Run:
            switch(ThisEvent.EventType){
                case ES_ENTRY:
                    RunS();
                    ES_Timer_InitTimer(DanceTimer, 8000);
                    ThisEvent.EventType = ES_NO_EVENT;
                    break;
                case BUMPER:
                    if(ThisEvent.EventParam < 4){
                        nextState = Backup;
                        makeTransition = TRUE;
                        BumperEvent = ThisEvent;
                    }
                    break;
                case ES_TIMEOUT:
                    if(ThisEvent.EventParam == DanceTimer){
                        nextState = Dance;
                        makeTransition = TRUE;
                    }
                    break;
                }
            break;
        case Backup:
            switch(ThisEvent.EventType){
                case ES_ENTRY:
                    BackUP();
                    ThisEvent.EventType = ES_NO_EVENT;
                    break;
                case ES_TIMEOUT:
                    if(ThisEvent.EventParam == HSMTime){
                        nextState = Turn;
                        makeTransition = TRUE;
                        
                    }
                    if(ThisEvent.EventParam == DanceTimer){
                        nextState = Dance;
                        makeTransition = TRUE;
                    }
                    break;
                    
                }
            break; 
        case Turn:
            switch(ThisEvent.EventType) {
                case ES_ENTRY:
                    TurnR();
                    ES_Timer_InitTimer(HSMTime, 500);
                    break;
                    
                case ES_TIMEOUT:
                    if (ThisEvent.EventParam == HSMTime) {
                        nextState = Run;
                        makeTransition = TRUE;
                    }
                    if(ThisEvent.EventParam == DanceTimer){
                        nextState = Dance;
                        makeTransition = TRUE;
                    }
                    break;
                    
            
            }
            break;
        case Dance:
            switch(ThisEvent.EventType) {
                case ES_ENTRY:
                    TurnR();
                case ES_TIMEOUT:
                    if(ThisEvent.EventParam == RandTimer){
                        nextState = Run;
                        makeTransition = TRUE;
                    }   
            }
            break;
        default:
            printf("\r\n Hit a snag Back to start");
                        nextState = Run;
                        makeTransition = TRUE;
                        break;
            
    } // end switch on Current State

    if (makeTransition == TRUE) { // making a state transition, send EXIT and ENTRY
        // recursively call the current state with an exit event
        RunRoachSubLightHSM(EXIT_EVENT); // <- rename to your own Run function
        CurrentState = nextState;
        RunRoachSubLightHSM(ENTRY_EVENT); // <- rename to your own Run function
    }

    ES_Tail(); // trace call stack end
    return ThisEvent;
}


/*******************************************************************************
 * PRIVATE FUNCTIONS                                                           *
 ******************************************************************************/

