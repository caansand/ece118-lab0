/*
 * File:   Lab0Main.c
 * Author: coope
 *
 * Created on April 6, 2024, 7:24 PM
 */


#include "xc.h"
#include <stdio.h>
#include <stdlib.h>
#include <BOARD.h>
#include <roach.h>

/*
 * 
 */
//#define Part2
//#define Part4


#ifdef Part2
int main(void) {
    BOARD_Init();
    Roach_Init();
    printf("Hello World");
    while(1);
    return ERROR;
}
#endif
#ifdef Part4
#define DELAY(x)    for (wait = 0; wait <= x; wait++) {asm("nop");}
#define A_BIT       18300
#define A_BIT_MORE  36600
#define YET_A_BIT_LONGER (A_BIT_MORE<<2)
#define A_LOT       183000
#define LOW_BAT 263
#define HIGH_BAT 310
#define FLEFT_BUMP_MASK (1)
#define FRIGHT_BUMP_MASK (1<<1)
#define RLEFT_BUMP_MASK (1<<2)
#define RRIGHT_BUMP_MASK (1<<3)
#define MOTOR_TIME (A_LOT<<2)

#define LIGHT_PAT_REPEAT 4
#define BAR_SIZE 13
#define motorSpeed0 0
#define motorSpeed1 50
#define motorSpeed2 -50
#define motorSpeed3 100

int main(void) {
    unsigned int wait;
    unsigned int scaledValue;
    BOARD_Init();
    Roach_Init();
    printf ("Howdy, welcome to the roach test case\r\n");
    printf ("The Leds will display a small pattern now\r\n");
    for (int i = 0; i < LIGHT_PAT_REPEAT; i++) {
        for (int j = 0; j < BAR_SIZE; j++){
            Roach_BarGraph(j);
            DELAY(A_BIT);
        }
        DELAY(A_LOT);
    }
    Roach_LEDSSet(0);
    printf("batery level showing\r\n");
    scaledValue = Roach_BatteryVoltage();
    scaledValue -= LOW_BAT;
    scaledValue *= 12;
    scaledValue /= (HIGH_BAT - LOW_BAT);
    Roach_BarGraph(scaledValue);
    DELAY(A_BIT_MORE);
    printf("Click the bumpers to run the tests\r\n");
    printf("Front Left: shows the battery level \r\n");
    printf("Front Right: show the light level\r\n");
    printf("Back Left: Test left motor\r\n");
    printf("Back Right: Test right motor\r\n");
    while(TRUE){
        switch (Roach_ReadBumpers()) {
        case FLEFT_BUMP_MASK: // Battery test
            DELAY(A_LOT);
            printf("Battery level is %d\r\n", Roach_BatteryVoltage());
            scaledValue = Roach_BatteryVoltage();
            scaledValue -= LOW_BAT;
            scaledValue *= 12;
            scaledValue /= (HIGH_BAT - LOW_BAT);
            Roach_BarGraph(scaledValue);
            printf("Test Done\r\n");
            break;
        case FRIGHT_BUMP_MASK: // light level
            DELAY(A_LOT);
            printf("Current Light Level: %d\r\n", Roach_LightLevel());
            scaledValue *= 12;
            scaledValue /= 1023;
            Roach_BarGraph(scaledValue);
            printf("Test Done\r\n");
        break;
        case RLEFT_BUMP_MASK: // Left motor
            DELAY(A_LOT);
            printf("motorSpeed at %d\r\n",motorSpeed1);
            Roach_LeftMtrSpeed(motorSpeed1);
            DELAY(MOTOR_TIME);
            printf("motorSpeed at %d\r\n",motorSpeed2);
            Roach_LeftMtrSpeed(motorSpeed2);
            DELAY(MOTOR_TIME);
            printf("motorSpeed at %d\r\n",motorSpeed3);
            Roach_LeftMtrSpeed(motorSpeed3);
            DELAY(MOTOR_TIME);
            printf("stopping\r\n");
            Roach_LeftMtrSpeed(motorSpeed0);
            DELAY(MOTOR_TIME);
            printf("Test Done\n");
            break;
        case RRIGHT_BUMP_MASK: // Right motor
            DELAY(A_LOT);
            printf("motorSpeed at %d\r\n",motorSpeed1);
            Roach_RightMtrSpeed(motorSpeed1);
            DELAY(MOTOR_TIME);
            printf("motorSpeed at %d\r\n",motorSpeed2);
            Roach_RightMtrSpeed(motorSpeed2);
            DELAY(MOTOR_TIME);
            printf("motorSpeed at %d\r\n",motorSpeed3);
            Roach_RightMtrSpeed(motorSpeed3);
            DELAY(MOTOR_TIME);
            printf("stopping\r\n");
            Roach_RightMtrSpeed(motorSpeed0);
            DELAY(MOTOR_TIME);
            printf("Test Done\r\n");
            break;
        }
    }
    while(1);
    return ERROR;
}
#endif